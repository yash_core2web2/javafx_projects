package com.c2w.SignUpPage;

import com.c2w.controller.AppNavigation;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class HospitalLogin  {

    private AppNavigation app;
    private GridPane gridPane;

    public HospitalLogin(AppNavigation app){
            this.app=app;
            initilize();
    }

    private void initilize(){
        
        TextField username = new TextField();
        username.setPromptText("Username/Email");
        username.setFont(Font.font("Arial", FontWeight.BOLD,15));
        
        //username.setStyle("-fx-text-fill:red;");
        //username.setStyle("-fx-background-color:#87986a");
        username.setPrefWidth(150);
        
        username.setPrefHeight(30);

        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        password.setFont(Font.font("Arial", FontWeight.BOLD,15));
        password.setStyle("-fx-text-fill:");
        password.setPrefHeight(30);
        password.setPrefWidth(250);
        

        Label loginLabel = new Label("     Login");
        loginLabel.setFont(new Font(40));
        loginLabel.setLayoutX(100);
        loginLabel.setLayoutY(100);

        loginLabel.setTextFill(Color.GREEN);

        loginLabel.setFont(Font.font("Arial", FontWeight.MEDIUM, 40));
         

        Button loginButton = new Button("  Login  ");
        Button signUpButton = new Button("  SignUp  ");

        loginButton.setFont(new Font(15));
        loginButton.setFont(Font.font("Arial", FontWeight.BOLD,15));

        //signUpButton.setStyle("-fx-prompt-text-fill:blue;");

        signUpButton.setFont(new Font(15));
        signUpButton.setFont(Font.font("Arial", FontWeight.BOLD,15));



        loginButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green; ");
        signUpButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.navigateToOption();
            }
            
        });
        

        HBox hBox = new HBox(80,loginButton,signUpButton);

        VBox vBox = new VBox(40,username,password,hBox);
        vBox.setLayoutX(50);
        vBox.setLayoutY(100);

        
       
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        // gridPane.add(username,15,25);
        gridPane.add(hBox,15,27);
        gridPane.add(loginLabel,15,15);
        gridPane.setPadding(new Insets(10));
        gridPane.add(vBox,15,20);
        gridPane.setStyle("-fx-background-image:url(images/dr17.png); -fx-background-repeat:no-repeat");

    }

    public GridPane getGridPane(){
        return gridPane;
    }
}
