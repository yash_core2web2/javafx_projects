package com.c2w.SignUpPage;

import com.c2w.controller.AppNavigation;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


public class HospitalSignUp   {

    private AppNavigation app;
    private GridPane gridPane;
    
    private Group gp;

    public HospitalSignUp(AppNavigation app){
            this.app=app;
            initilize();
    }

    private void initilize(){

        Text txt2 = new Text("   ");

        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        gridPane.setStyle("-fx-background-image:url(images/dr17.png); -fx-background-repeat:no-repeat");
        gridPane.add(txt2,150,100);
    

        
        Text signUpText = new Text("    SignUp");
        signUpText.setFont(new Font(40));
        signUpText.setFill(Color.GREEN);
        signUpText.setFont(Font.font("STYLESHEET_CASPIAN",FontWeight.MEDIUM, 40));

        TextField hospitalNameField = new TextField();
        hospitalNameField.setPromptText("Hospital Name");

        TextField addressField = new TextField();
        addressField.setPromptText("Address");
        
        TextField cityField = new TextField(); 
        cityField.setPromptText("City");

        TextField stateField = new TextField();
        stateField.setPromptText("State");

        TextField pincodeField = new TextField();
        pincodeField.setPromptText("Pincode");

        TextField emailField = new TextField();
        emailField.setPromptText("Email");

        PasswordField passworField = new PasswordField();
        passworField.setPromptText("Password");

        PasswordField confirmPasswordField = new PasswordField();
        confirmPasswordField.setPromptText("Confirm Password");
        confirmPasswordField.setPrefWidth(250);

        Button signUpButton = new Button("SignUp");
        signUpButton.setFont(new Font(15));
        signUpButton.setLayoutX(200);
        signUpButton.setLayoutY(500);
        signUpButton.setFont(Font.font("Arial", FontWeight.BOLD,15));
        signUpButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

        Text txt = new Text("Already have an account");
        txt.setFont(new Font(15));
        txt.setLayoutX(120);
        txt.setLayoutY(590);

       

        Button loginButton = new Button("Login");
        loginButton.setFont(new Font(10));
        loginButton.setLayoutX(290);
        loginButton.setLayoutY(575);
        loginButton.setFont(Font.font("Arial", FontWeight.BOLD,10));
        //loginButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.navigateToHospitalLogin();
            }
            
        });

        

        VBox vb = new VBox(20,signUpText,hospitalNameField,addressField,cityField,stateField,pincodeField,emailField,passworField,confirmPasswordField);
        vb.setLayoutX(120);
        vb.setLayoutY(50);

        /*HBox hBox = new HBox(100,signUpButton);
        hBox.setLayoutX(120);
        hBox.setLayoutY(480);*/

       
        // gridPane.add(signUpText,11,8); 
        // gridPane.add(hospitalNameField,11,12);
        // gridPane.add(addressField,11,14);
        // gridPane.add(cityField,11,16);
        // gridPane.add(stateField,11,18);
        // gridPane.add(pincodeField,11,20);
        // gridPane.add(emailField,11,22);
        // gridPane.add(passworField,11,24);
        // gridPane.add(confirmPasswordField,11,26);
        // gridPane.add(hBox,11,30);
        // //gridPane.add(txt,11,32);
       

        gp = new Group(gridPane,vb,signUpButton,txt,loginButton);


    }

    public Group getGroup(){
        return gp;
    }
}

