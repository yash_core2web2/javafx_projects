package com.c2w.controller;
import com.c2w.SignUpPage.HospitalLogin;
import com.c2w.SignUpPage.HospitalSignUp;
import com.c2w.SignUpPage.Option;
import com.c2w.SignUpPage.PatientSignUp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppNavigation extends Application {
    private Stage primaryStage;
    private Scene hospitalLoginScene,hospitalSignUpScene,optionScene,patientScene;

    
    private HospitalLogin loginPage;
    
    private HospitalSignUp hospSignUpPage;

    private Option optionPage;

    private PatientSignUp patientSignUpPage;

    //PAge3 instance 
    //private FormPage3 page3;

    @Override
    public  void start(Stage primaryStage){
        this.primaryStage=primaryStage;

        
        loginPage = new HospitalLogin(this);
        hospSignUpPage = new HospitalSignUp(this);
        optionPage = new Option(this);
        patientSignUpPage = new PatientSignUp(this);
        //page3 = new FormPage3(this);

        hospitalLoginScene = new Scene(loginPage.getGridPane(),1285,720);
        hospitalSignUpScene = new Scene(hospSignUpPage.getGroup(),1285,720);
        optionScene = new Scene(optionPage.getGroup(),1285,720);
        patientScene = new Scene(patientSignUpPage.getGroup(),1285,720);
        //page3Scene = new Scene(page3.getView(),400,300);

        primaryStage.setScene(hospitalLoginScene);
        //primaryStage.setMaximized(true);
        primaryStage.setTitle("App");
        primaryStage.show();

    }

    public void navigateToHospitalLogin(){
       
        primaryStage.setScene(hospitalLoginScene); 
          
    }

    public void navigateToOption(){
        primaryStage.setScene(optionScene);
    }
    public void navigateToHospitalSignUp(){
       
        primaryStage.setScene(hospitalSignUpScene);
    
        
    }
    public void navigateToPatientSignUp(){
        primaryStage.setScene(patientScene);
    }


    
}
