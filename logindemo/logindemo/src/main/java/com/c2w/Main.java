package com.c2w;

import com.c2w.Controller.AppNavigation;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        Application.launch(AppNavigation.class,args);
    }
}