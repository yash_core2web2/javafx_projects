package com.c2w.Controller;

import com.c2w.firebaseConfig.DataService;
//import com.c2w.firebaseConfig.DataService;
import com.c2w.hospital_dashboard.Doctors;
import com.c2w.hospital_dashboard.Information;
import com.c2w.hospital_dashboard.Pending;
import com.c2w.hospital_dashboard.Previous;
import com.c2w.patient_dashboard.Book_New_Appointment;
import com.c2w.patient_dashboard.Previous_Appointments;
import com.c2w.patient_dashboard.Upcoming_Appointments;
import com.c2w.signup.HospitalLogin;
import com.c2w.signup.HospitalSignUp;
import com.c2w.signup.Option;
import com.c2w.signup.PatientSignUp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppNavigation extends Application {
    private Stage primaryStage;
    private Scene loginScene,optioScene,hsignupScene,psignupScene;
    private HospitalLogin loginpage;
    private HospitalSignUp hsignuppage;
    private Option optionpage;
    private PatientSignUp psignuppage;

    private Scene informationScene,pendingScene,preiviouScene,doctorScene;
    private Information informationpage;
    private Pending pendingpage;
    private Previous previouspage;
    private Doctors doctorpage;

   private Scene bookScene,previous_appointScene,upcomingScene;
    private Book_New_Appointment bookpage;
    private Previous_Appointments previous_appointpage;
    private  Upcoming_Appointments upcomingpage;

    private int x = 0;




    @Override
    public void start(Stage primaryStage)  {

        this.primaryStage = primaryStage;

        loginpage = new HospitalLogin(this);
        hsignuppage = new HospitalSignUp(this);
        optionpage = new Option(this);
        psignuppage = new PatientSignUp(this);

        informationpage = new Information(this);
        pendingpage = new Pending(this);
        previouspage = new Previous(this);
        doctorpage = new Doctors(this);

        bookpage = new Book_New_Appointment(this);
        previous_appointpage = new Previous_Appointments(this);
        upcomingpage = new Upcoming_Appointments(this);


         loginScene = new Scene(loginpage.getView(),1920,1080);
         hsignupScene = new Scene(hsignuppage.getView(),1920,1080);
         optioScene = new Scene(optionpage.getView(),1920,1080);
         psignupScene = new Scene(psignuppage.getView(),1920,1080);

        // informationScene = new Scene(informationpage.getView(),1920,1080);
        // pendingScene = new Scene(pendingpage.getView(),1920,1080);
       // preiviouScene = new Scene(previouspage.getView(),1920,1080);
        //doctorScene = new Scene(doctorpage.getView(),1920,1080);

        // bookScene = new Scene(bookpage.getView(),1920,1080);
        // previous_appointScene = new Scene(previous_appointpage.getView(),1920,1080);
       // upcomingScene = new Scene(upcomingpage.getView(),1920,1080);


        primaryStage.setScene(loginScene);
        primaryStage.setMaximized(true);
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public void NavigateToLogin(){
        loginpage.initilize();
        primaryStage.setScene(loginScene);
    }

    public void navigateToOption(){
        optionpage.initilize();
        primaryStage.setScene(optioScene);
    }
    public void NavigateToHsignup(){
        hsignuppage.initilize();
        primaryStage.setScene(hsignupScene);
    }
    public void NavigateToPsignup(){
        pendingpage.initialize();
        primaryStage.setScene(psignupScene);
    }

    public void NavigateToInformation() {
        //informationScene = new Scene(informationpage.getView(),1920,1080);
        informationpage.initialize();
        informationScene = new Scene(informationpage.getView(),1920,1080);
        primaryStage.setScene(informationScene);
    }

    public void NavigateFromDoctorsToInformation() {
        primaryStage.setScene(informationScene);
    }
    public void NavigateToPending() {
        pendingpage.initialize();
        pendingScene = new Scene(pendingpage.getView(),1920,1080);
       primaryStage.setScene(pendingScene);
    }
    public void NavigateToPrevious() {
        previouspage.initialize();
        preiviouScene = new Scene(previouspage.getView(),1920,1080);
        primaryStage.setScene(preiviouScene);
    }
    public void NavigateToDoctors() {
        if(x==0){
        doctorpage.initialize();
        
        doctorScene = new Scene(doctorpage.getView(),1920,1080);
       primaryStage.setScene(doctorScene);
       x++;
     }else{
        primaryStage.setScene(doctorScene);
     }

    }
    public void NavigateToBook() {
        bookpage.initialize();
        bookScene = new Scene(bookpage.getView(),1920,1080);
       primaryStage.setScene(bookScene);
    }
    public void NavigateToPreviousAppoint() {
        previous_appointpage.initialize();
        previous_appointScene = new Scene(previous_appointpage.getView(),1920,1080);
       primaryStage.setScene(previous_appointScene);
    }
    public void NavigateToUpcoming() {
       upcomingpage.initialize();
       upcomingScene = new Scene(upcomingpage.getView(),1920,1080);
       primaryStage.setScene(upcomingScene);
    }
}
