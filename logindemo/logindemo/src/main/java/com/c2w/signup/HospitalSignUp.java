package com.c2w.signup;

import java.util.HashMap;
import java.util.Map;


import com.c2w.Controller.AppNavigation;
import com.c2w.firebaseConfig.DataService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


public class HospitalSignUp   {

    private AppNavigation app;
    private GridPane view1;
    
    private Group view;

    public HospitalSignUp(AppNavigation app){
            this.app=app;
            initilize();
    }

    public void initilize(){

        Text txt2 = new Text("   ");

        view1 = new GridPane();
        view1.setPadding(new Insets(10));
        view1.setHgap(10);
        view1.setVgap(10);

        view1.setStyle("-fx-background-image:url(images/dr17.png); -fx-background-repeat:no-repeat");
        view1.add(txt2,150,100);
    

        
        Text signUpText = new Text("    SignUp");
        signUpText.setFont(new Font(40));
        signUpText.setFill(Color.GREEN);
        signUpText.setFont(Font.font("STYLESHEET_CASPIAN",FontWeight.MEDIUM, 40));

        TextField hospitalNameField = new TextField();
        hospitalNameField.setPromptText("Hospital Name");

        TextField addressField = new TextField();
        addressField.setPromptText("Address");
        
        TextField cityField = new TextField(); 
        cityField.setPromptText("City");

        TextField stateField = new TextField();
        stateField.setPromptText("State");

        TextField pincodeField = new TextField();
        pincodeField.setPromptText("Pincode");

        TextField emailField = new TextField();
        emailField.setPromptText("Email");

        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("Password");

        PasswordField confirmPasswordField = new PasswordField();
        confirmPasswordField.setPromptText("Confirm Password");
        confirmPasswordField.setPrefWidth(250);

        Button signUpButton = new Button("SignUp");
        signUpButton.setFont(new Font(15));
        signUpButton.setLayoutX(200);
        signUpButton.setLayoutY(530);
        signUpButton.setFont(Font.font("Arial", FontWeight.BOLD,15));
        signUpButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

        Text txt = new Text("Already have an account");
        txt.setFont(new Font(15));
        txt.setLayoutX(120);
        txt.setLayoutY(590);

       

        Button loginButton = new Button("Login");
        loginButton.setFont(new Font(10));
        loginButton.setLayoutX(290);
        loginButton.setLayoutY(575);
        loginButton.setFont(Font.font("Arial", FontWeight.BOLD,10));
        //loginButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.NavigateToLogin();
            }
        });

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if(passwordField.getText().equals(confirmPasswordField.getText())){
               handleSignup(hospitalNameField.getText(),addressField.getText(),cityField.getText(),stateField.getText(),pincodeField.getText(),emailField.getText(),passwordField.getText());
            }else {
                    System.out.println("Passwords misMatched");
            }
         }
        });
        

        

        VBox vb = new VBox(20,signUpText,hospitalNameField,addressField,cityField,stateField,pincodeField,emailField,passwordField,confirmPasswordField);
        vb.setLayoutX(120);
        vb.setLayoutY(50);

       

        view = new Group(view1,vb,signUpButton,txt,loginButton);


    }

    public Group getView(){
        return view;
    }

    private void handleSignup(String hospitalName,String address,String city,String state,String pincode,String email,String password) {
        DataService dataService;
        try {
            dataService = new DataService();
            Map<String, Object> data = new HashMap<>();
            data.put("password",password);
            data.put("hospital name", hospitalName);
            data.put("address",address);
            data.put("city",city);
            data.put("state",state);
            data.put("pin code",pincode);
            data.put("email",email);
            //dataService.addData("hospitals", email,data);
            dataService.addData("hospitals", hospitalName,data);
            System.out.println("Hospital registered successfully");
            app.NavigateToLogin();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
}

}