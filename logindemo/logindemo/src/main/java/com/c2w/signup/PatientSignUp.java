package com.c2w.signup;

import java.util.HashMap;
import java.util.Map;

import com.c2w.Controller.AppNavigation;
import com.c2w.firebaseConfig.DataService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


public class PatientSignUp {
  
  private AppNavigation app;
  private Group view ;
  public PatientSignUp(AppNavigation app){
    this.app=app;
    initilize();
  }
  public void initilize(){
   

    GridPane gridPane = new GridPane();
    gridPane.setPadding(new Insets(20)); // Padding around the grid
    gridPane.setHgap(10); // Horizontal gap between columns
    gridPane.setVgap(10); // Vertical gap between rows

    Label titleLabel = new Label("SignUp");
    titleLabel.setTextFill(Color.DARKGREEN);
    titleLabel.setFont(new Font(40));
    titleLabel.setLayoutX(140);
  //  GridPane.setConstraints(titleLabel, 6, 4); 


    Label nameLabel = new Label("Name:");
    nameLabel.setFont(new Font(17));
    GridPane.setConstraints(nameLabel, 6, 10); // (column, row)

    Label numLabel = new Label("Mobile Number:");
    numLabel.setFont(new Font(17));
    GridPane.setConstraints(numLabel, 6, 11);


    Label emailLabel = new Label("Email_Id:");
    emailLabel.setFont(new Font(17));
    GridPane.setConstraints(emailLabel, 6, 12);

    Label passLabel = new Label("Password:");
    passLabel.setFont(new Font(17));
    GridPane.setConstraints(passLabel, 6, 13);

    Label confpassLabel = new Label("Confirm Password:");
    confpassLabel.setFont(new Font(17));
    GridPane.setConstraints(confpassLabel, 6, 14);

    TextField nameField = new TextField();
    GridPane.setConstraints(nameField, 7, 10);
    nameField.setPromptText("Name");

    TextField numField = new TextField();
    GridPane.setConstraints(numField, 7, 11);
    numField.setPromptText("Moblie Number");
    
    TextField mailField = new TextField();
    GridPane.setConstraints(mailField, 7, 12);
    mailField.setPromptText("Email Id");

    PasswordField passField = new PasswordField();
    GridPane.setConstraints(passField, 7, 13);
    passField.setPromptText("Password");

    PasswordField conpassField = new PasswordField();
    GridPane.setConstraints(conpassField, 7, 14);
    conpassField.setPromptText("Confirm Password");

    Button signuupButton = new Button(" SignUp ");
    signuupButton.setLayoutX(100);
    signuupButton.setLayoutY(335);
    signuupButton.setFont(new Font(15));
    signuupButton.setFont(Font.font("Arial",FontWeight.BOLD,15));

    Button loginButton = new Button("Login");
    loginButton.setFont(new Font(10));
    loginButton.setLayoutX(240);
    loginButton.setLayoutY(335);
    loginButton.setFont(Font.font("Arial", FontWeight.BOLD,15));

    loginButton.setOnAction(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
          app.NavigateToLogin();
      }
      
    });
    signuupButton.setOnAction(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        if(passField.getText().equals(conpassField.getText())){
          handleSignup(nameField.getText(),numField.getText(),mailField.getText(),passField.getText());
       }else {
               System.out.println("Passwords misMatched");
       }
      }
      
    });
    


    Text txt = new Text(" ");
    gridPane.getChildren().addAll(nameLabel, emailLabel, numLabel,passLabel,confpassLabel,nameField, numField,passField ,mailField,conpassField);
    gridPane.setStyle("-fx-background-image:url(images/dr17.png); -fx-background-repeat:no-repeat");
    gridPane.add(txt,110,70);
    view=new Group(gridPane,titleLabel,signuupButton,loginButton);

  }

  private void handleSignup(String username,String mobnumber,String email,String password) {
        DataService dataService;
        try {
            dataService = new DataService();
            Map<String, Object> data = new HashMap<>();
            data.put("password",password);
            data.put("username", username);
            data.put("email",email);
            data.put("phone number",mobnumber);
            dataService.addData("patients", username,data);
            System.out.println("Patient registered successfully");
            app.NavigateToLogin();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
}
        
    public Group getView(){
      return view;
    }
    

}
