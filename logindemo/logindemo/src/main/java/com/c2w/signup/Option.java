package com.c2w.signup;

import com.c2w.Controller.AppNavigation;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Option {
    private AppNavigation app;
    private GridPane gridPane;
    private Group view;

    public Option(AppNavigation app){
        this.app = app;
        initilize();
    }

    public void initilize(){

        Text txt2 = new Text(" kbvjjjjjjjjjjjjjjjjjjjjjjjjjj  ");

        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        gridPane.setStyle("-fx-background-image:url(images/dr17.png); -fx-background-repeat:no-repeat");
        gridPane.add(txt2,150,100);

        Text label = new Text("SignUp as");
        label.setFont(new Font(40));
        label.setLayoutX(180);
        label.setLayoutY(200);
        label.setFill(Color.GREEN);

        label.setFont(Font.font("", FontWeight.MEDIUM, 40));

        Button patient = new Button("Patient");
        Button hospital = new Button("Hospital");
        Button backButton = new Button("Back");

        patient.setLayoutX(150);
        patient.setLayoutY(300);
        patient.setFont(new Font(10));
        patient.setFont(Font.font("Arial", FontWeight.BOLD,20));
        patient.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

        hospital.setFont(new Font(15));
        hospital.setLayoutX(300);
        hospital.setLayoutY(300);
        hospital.setFont(Font.font("Arial", FontWeight.BOLD,20));
        hospital.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

       
        patient.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.NavigateToPsignup();
            }
            
        });

        hospital.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
              app.NavigateToHsignup();
            }
            
        });

       Image backImage = new Image("images/backarrow.jpg");
       ImageView backImageView = new ImageView(backImage);
       backImageView.setFitHeight(30);
       backImageView.setFitWidth(30);
       backButton = new Button();
       backButton.setGraphic(backImageView);
       backButton.setLayoutX(10);
       backButton.setLayoutY(10);

        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
                app.NavigateToLogin();
            }
        });


        view = new Group(gridPane,label,patient,hospital,backButton);
    
    }

    public Group getView(){
        return view;
    }
}
