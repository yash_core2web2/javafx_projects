package com.c2w.signup;

import java.util.concurrent.ExecutionException;

import com.c2w.Controller.AppNavigation;
import com.c2w.firebaseConfig.DataService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class HospitalLogin  {

    private AppNavigation app;
    private GridPane view;
    private DataService dataService;
    public static String key;
    public static String mainkey;

    public HospitalLogin(AppNavigation app){
            this.app=app;
            initilize();
    }

    public void initilize(){
        dataService = new DataService();
        TextField email = new TextField();
        email.setPromptText("Hospital Name/User Name");
        email.setFont(Font.font("Arial", FontWeight.BOLD,15));
        
        //username.setStyle("-fx-text-fill:red;");
        //username.setStyle("-fx-background-color:#87986a");
        email.setPrefWidth(150);
        
        email.setPrefHeight(30);

        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        password.setFont(Font.font("Arial", FontWeight.BOLD,15));
        password.setStyle("-fx-text-fill:");
        password.setPrefHeight(30);
        password.setPrefWidth(250);
        

        Label loginLabel = new Label("     Login");
        loginLabel.setFont(new Font(40));
        loginLabel.setLayoutX(100);
        loginLabel.setLayoutY(100);

        loginLabel.setTextFill(Color.GREEN);

        loginLabel.setFont(Font.font("Arial", FontWeight.MEDIUM, 40));
         

        Button loginButton = new Button("  Login  ");
        Button signUpButton = new Button("  SignUp  ");

        loginButton.setFont(new Font(15));
        loginButton.setFont(Font.font("Arial", FontWeight.BOLD,15));

        //signUpButton.setStyle("-fx-prompt-text-fill:blue;");

        signUpButton.setFont(new Font(15));
        signUpButton.setFont(Font.font("Arial", FontWeight.BOLD,15));



        loginButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green; ");
        signUpButton.setStyle("-fx-background-color:#87986a ; -fx-text-fill:green;");

        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                handleLogin(email.getText(),password.getText());
                email.clear();
                password.clear();
            }
            
        });

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               app.navigateToOption();
               email.clear();
               password.clear();
            }    
        });

        HBox hBox = new HBox(80,loginButton,signUpButton);

        VBox vBox = new VBox(40,email,password,hBox);
        vBox.setLayoutX(50);
        vBox.setLayoutY(100);

        
       
        view = new GridPane();
        view.setHgap(10);
        view.setVgap(10);
        // gridPane.add(username,15,25);
        view.add(hBox,15,27);
        view.add(loginLabel,15,15);
        view.setPadding(new Insets(10));
        view.add(vBox,15,20);
        view.setStyle("-fx-background-image:url(images/dr17.png); -fx-background-repeat:no-repeat");

    }

    private void handleLogin(String hname,String password) {
        try {
            if (dataService.authenticateHospital(hname, password)) {
                key = hname;
                keygiver(mainkey);
                System.out.println("key == "+key);
                System.out.println("Hospital varified");
                app.NavigateToInformation();
            }else if(dataService.authenticatePatient(hname, password)) {
                key = hname;
                keygiver(mainkey);
                System.out.println("key == "+key);
                app.NavigateToBook();
                System.out.println("Patient Variefied");
            } else {
                System.out.println("Invalid credentials");
                key = null;
            }
        } catch (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
            System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        }
    }
    public static String keygiver(String setterkey) {
           setterkey = key;
           return setterkey; 
    }

    public GridPane getView(){
        return view;
    }
}
