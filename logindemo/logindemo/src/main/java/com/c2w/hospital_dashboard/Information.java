package com.c2w.hospital_dashboard;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.c2w.Controller.AppNavigation;
import com.c2w.firebaseConfig.DataService;
import com.c2w.signup.HospitalLogin;
import com.google.cloud.firestore.DocumentSnapshot;


import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.geometry.Pos;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Popup;


public class Information {

    private AppNavigation app;
    private Pane view;
    Image bgImage;
    BackgroundImage backgroundImage ;
    Button information_button;
    Button pending_appointment;
    Button completed_appointment;
    Button list_doctor;
    Label sit_cap;
    Label appointment_cap;
    TextField sitTField;
    TextField appointmentTField;
    Button logout;
    Button about_us;
    private String key;

    TextField timeField;
    TextField timeField1;
    DatePicker datePicker;
    private DataService dataService;
    Label dataLabel;

   // private TableView<Doctors> doctorTableView;

    
     


    public Information(AppNavigation app) {
        this.app = app;
        //this.dataService = new DataService();
        //initialize();
    }
    public Pane getView() {
        return view;
    }

    public void initialize() {
        this.dataService = new DataService();
        System.out.println("vjhgggggggggggggggggggggggvgvgggggggggggggg");

       bgImage = new Image("images/d3.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
       Background background = new Background(backgroundImage);
       
       information_button = new Button("Information");
       pending_appointment = new Button("Pending Appointments");
       completed_appointment = new Button("Completed Appointments");
       information_button.setPrefWidth(640);
       pending_appointment.setPrefWidth(640);
       completed_appointment.setPrefWidth(640);
       information_button.setPrefHeight(55);
       pending_appointment.setPrefHeight(55);
       completed_appointment.setPrefHeight(55);
       information_button.setStyle("-fx-font-weight:bold");
       pending_appointment.setStyle(" -fx-font-weight:bold");
       completed_appointment.setStyle("-fx-font-weight:bold");

       completed_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToPrevious();
        }
       });
       pending_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToPending();
        }
       });
       
       HBox hBox = new HBox(information_button,pending_appointment,completed_appointment);
       hBox.setLayoutY(993);

       datePicker = new DatePicker();
       datePicker.setValue(LocalDate.now());
       datePicker.setPrefWidth(175);
       datePicker.setPrefHeight(35);
       datePicker.setStyle("-fx-font-weight:bold");

//********************************************************************************************************** */
        timeField = new TextField();
        timeField.setPromptText("HH:MM AM/PM");
        timeField.setEditable(false);
        Button openButton = new Button("Select Opening Time");
        ComboBox<Integer> hourComboBox = new ComboBox<>();
        for (int i = 1; i <= 12; i++) {
            hourComboBox.getItems().add(i);
        }
        ComboBox<Integer> minuteComboBox = new ComboBox<>();
        for (int i = 0; i < 60; i++) {
            minuteComboBox.getItems().add(i);
        }
        ComboBox<String> amPmComboBox = new ComboBox<>();
        amPmComboBox.getItems().addAll("AM", "PM");
        amPmComboBox.setValue("AM");

        HBox timePickerBox = new HBox(10, hourComboBox, new Label(":"), minuteComboBox, amPmComboBox);
        timePickerBox.setLayoutY(115);
        timePickerBox.setLayoutX(130);
    

        Popup popup = new Popup();
        popup.getContent().add(timePickerBox);
        popup.setAutoHide(true);

        openButton.setOnAction(e -> {
            if (!popup.isShowing()) {
                popup.show(view.getScene().getWindow()); 
            }
        });


        ChangeListener<Object> timeChangeListener = (observable, oldValue, newValue) -> {
            if (hourComboBox.getValue() != null && minuteComboBox.getValue() != null && amPmComboBox.getValue() != null) {
                int hour = hourComboBox.getValue();
                int minute = minuteComboBox.getValue();
                String amPm = amPmComboBox.getValue();
                String formattedTime = String.format("%02d:%02d %s", hour, minute, amPm);
                timeField.setText(formattedTime);

                popup.hide(); 
            }
        };

        hourComboBox.valueProperty().addListener(timeChangeListener);
        minuteComboBox.valueProperty().addListener(timeChangeListener);
        amPmComboBox.valueProperty().addListener(timeChangeListener);

        VBox root = new VBox(10, timeField, openButton);
        root.setAlignment(Pos.CENTER);

//*********************************************************************************************************** */
        timeField1 = new TextField();
        timeField1.setPromptText("HH:MM AM/PM");
        timeField1.setEditable(false);
        Button openButton1 = new Button("Select Closing Time");
        ComboBox<Integer> hourComboBox1 = new ComboBox<>();
        for (int i = 1; i <= 12; i++) {
            hourComboBox1.getItems().add(i);
        }
        ComboBox<Integer> minuteComboBox1 = new ComboBox<>();
        for (int i = 0; i < 60; i++) {
            minuteComboBox1.getItems().add(i);
        }
        ComboBox<String> amPmComboBox1 = new ComboBox<>();
        amPmComboBox1.getItems().addAll("AM", "PM");
        amPmComboBox1.setValue("AM");

        HBox timePickerBox1 = new HBox(10, hourComboBox1, new Label(":"), minuteComboBox1, amPmComboBox1);
        timePickerBox1.setLayoutY(115);
        timePickerBox1.setLayoutX(910);
    

        Popup popup1 = new Popup();
        popup1.getContent().add(timePickerBox1);
        popup1.setAutoHide(true);

        openButton1.setOnAction(e -> {
            if (!popup1.isShowing()) {
                popup1.show(view.getScene().getWindow()); 
            }
        });


        ChangeListener<Object> timeChangeListener1 = (observable, oldValue, newValue) -> {
            if (hourComboBox1.getValue() != null && minuteComboBox1.getValue() != null && amPmComboBox1.getValue() != null) {
                int hour = hourComboBox1.getValue();
                int minute = minuteComboBox1.getValue();
                String amPm = amPmComboBox1.getValue();
                String formattedTime = String.format("%02d:%02d %s", hour, minute, amPm);
                timeField1.setText(formattedTime);

                popup1.hide(); 
            }
        };

        hourComboBox1.valueProperty().addListener(timeChangeListener1);
        minuteComboBox1.valueProperty().addListener(timeChangeListener1);
        amPmComboBox1.valueProperty().addListener(timeChangeListener1);

        VBox root1 = new VBox(10, timeField1, openButton1);
        root1.setAlignment(Pos.CENTER);

//*********************************************************************************************************** */
        HBox timehHBox = new HBox(200,root,root1);
        timehHBox.setPrefWidth(600);
        timehHBox.setAlignment(Pos.CENTER);

       VBox vBox = new VBox(40,datePicker,timehHBox);
       vBox.setPrefWidth(600);
       vBox.setAlignment(Pos.CENTER);
       vBox.setLayoutY(150);
       vBox.setLayoutX(900);

       sit_cap = new Label("Sitting Capacity");
       appointment_cap = new Label("Appointment Capacity of a Slot");
       sitTField = new TextField();
       appointmentTField = new TextField();
       HBox sitHBox = new HBox(115,sit_cap,sitTField);
       HBox appointmentHBox = new HBox(10,appointment_cap,appointmentTField);

       sitHBox.setPrefWidth(600);
       //sitHBox.setAlignment(Pos.CENTER);
       appointmentHBox.setPrefWidth(600);
       //appointmentHBox.setAlignment(Pos.CENTER);

       list_doctor = new Button("List Available Doctors Today");
       list_doctor.setPrefHeight(35);

       list_doctor.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            app.NavigateToDoctors();
        }
       });
       VBox vb1 = new VBox(50,list_doctor,sitHBox,appointmentHBox);
       vb1.setAlignment(Pos.CENTER);

       VBox totalVBox = new VBox(80,vBox,vb1);
       totalVBox.setPrefWidth(600);
       totalVBox.setAlignment(Pos.CENTER);
       totalVBox.setLayoutY(225);
       totalVBox.setLayoutX(900);

       logout = new Button("Logout");
       logout.setPrefHeight(35);
       about_us = new Button("About Us");
       about_us.setPrefHeight(35);

       logout.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
           app.NavigateToLogin();
        }
        
       });

       HBox outHBox = new HBox(5,about_us,logout);
       outHBox.setLayoutX(1750);
       outHBox.setLayoutY(850);

       Text welcome = getLoginName();
       welcome.setLayoutY(15);
       welcome.setLayoutX(5);
       welcome.setFont(new Font("Cooper Black", 55));
       welcome.setFill(Color.BROWN);

       HBox welcomeHBox = new HBox(welcome);
       welcomeHBox.setPrefWidth(1920);
       welcomeHBox.setAlignment(Pos.CENTER);

       Button confirm = new Button("Confirm");
       confirm.setPrefHeight(35);

       confirm.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            handleConfirm();
        }    
       });

       Button clear = new Button("Clear");
       clear.setPrefHeight(35);

       clear.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            timeField.clear();
            timeField1.clear();
            sitTField.clear();
            appointmentTField.clear();

        }
        
       });

       HBox conclear = new HBox(10,confirm,clear);
       conclear.setPrefWidth(600);
       conclear.setAlignment(Pos.CENTER);
       conclear.setLayoutX(900);
       conclear.setLayoutY(700);
       
       view = new Pane(welcomeHBox,hBox,totalVBox,outHBox,conclear);
       view.setBackground(background);
}

     public Text getLoginName(){
        dataLabel = new Label();
        String hospitalName = "default name";
        String yashkey = null;
        key = HospitalLogin.keygiver(yashkey);
        System.out.println(key);
        try{
            if (key != null) {
                DocumentSnapshot dataObject = dataService.getData("hospitals", key);
                hospitalName = dataObject.getString("hospital name");
            } else {
                System.out.println("Key is null.");
            }
            dataLabel.setText(hospitalName);
        }catch(Exception e){
             e.printStackTrace();
        }
        return new Text("Welcome to "+dataLabel.getText());

    //     Label dataLabel = new Label("");
    //     try {
    //        String key = HospitalLogin.key;
    //         System.out.println("Value of key: " + key);
    //         DocumentSnapshot dataObject = dataService.getData("hospitals", key);
    //        String hospitalName = dataObject.getString("hospital name");
    //        System.out.println("Username fetched: " + hospitalName);
    //        dataLabel.setText(hospitalName) ;
    //     } catch (Exception ex) {
    //     ex.printStackTrace();
    //   }
    //       return new Text("Welcome " + dataLabel.getText());
    }

    private void handleConfirm() {
        LocalDate date = datePicker.getValue();
        String openingTime = timeField.getText();
        String closingTime = timeField1.getText();
        String sittingCapacity = sitTField.getText();
        String appointmentCapacity = appointmentTField.getText();
        String hospitalName = key; 

        if(key != null) {

            try {
                Map<String, Object> data = new HashMap<>();
                data.put("openingTime", openingTime);
                data.put("closingTime", closingTime);
                data.put("sittingCapacity", sittingCapacity);
                data.put("appointmentCapacity", appointmentCapacity);
                data.put("Doctors", getDoctorsData());

                String dateString = date.toString();
                dataService.addSubDocument("hospitals", hospitalName, "dates", dateString, data);
                System.out.println("Data added successfully for date: " + dateString);

        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }else {
        System.out.println("hospital email not found");
    }
  }

    private ObservableList<Map<String, String>> getDoctorsData() {
       
        ObservableList<Doctors> doctorsList = Doctors.table.getItems();
        return doctorsList.stream().map(doctor -> {
            Map<String, String> doctorData = new HashMap<>();
            doctorData.put("ID", doctor.getId());
            doctorData.put("Name", doctor.getName());
            doctorData.put("Specialization", doctor.getSpecialization());
            doctorData.put("Degree", doctor.getDegree());
            return doctorData;
        }).collect(Collectors.toCollection(FXCollections::observableArrayList));
    }
}
