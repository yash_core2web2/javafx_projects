package com.c2w.firebaseConfig;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firestore.v1.QueryProto;
import com.google.cloud.firestore.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;



public class DataService {

        


    private static Firestore db;

    static {
        try {
            initializeFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void initializeFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("src\\main\\resources\\javafx-firestore-5d30b-firebase-adminsdk-lnj57-5da3f5393c.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .build();

        FirebaseApp.initializeApp(options);
        db = FirestoreClient.getFirestore();
    }

    public void addData(String collection, String document, Map<String, Object> data)
        throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection(collection).document(document);
        ApiFuture<WriteResult> result = docRef.set(data);
        result.get(); // Block until complete
    }

    public DocumentSnapshot getData(String collection, String document) throws
        ExecutionException, InterruptedException {
        try {
            DocumentReference docRef = db.collection(collection).document(document);
            ApiFuture<DocumentSnapshot> future = docRef.get();
            return future.get();
        } catch (Exception e) {
            e.printStackTrace(); // Example: Print the stack trace for debugging
            throw e; // Re-throw the exception or handle it based on your application's needs
        }
    }

    public boolean authenticateHospital(String hname, String password) throws
        ExecutionException, InterruptedException {
        DocumentSnapshot document = db.collection("hospitals").document(hname).get().get();
        if (document.exists()) {
            String storedPassword = document.getString("password");
            return password.equals(storedPassword);
        }
        return false;
    }
    public boolean authenticatePatient(String pname, String password) throws
        ExecutionException, InterruptedException {
        DocumentSnapshot document = db.collection("patients").document(pname).get().get();
        if (document.exists()) {
            String storedPassword = document.getString("password");
            return password.equals(storedPassword);
        }
        return false;
    }



    public void deleteProject(String collectionName, String leaderName) throws
        ExecutionException, InterruptedException {
        System.out.println(leaderName);
        ApiFuture<WriteResult> writeResult = db.collection(collectionName).document(leaderName).delete();
        writeResult.get();
    }

    public List<Map<String, Object>> getDataInDescendingOrder(String collectionName,
        String orderByField) throws ExecutionException, InterruptedException {
        List<Map<String, Object>> documentsList = new ArrayList<>();

        // Create a query against the collection
        CollectionReference collection = db.collection(collectionName);
        Query query = collection.orderBy(orderByField, Query.Direction.DESCENDING);

        // Execute the query
        ApiFuture<QuerySnapshot> querySnapshot = query.get();

        for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
            documentsList.add(document.getData());
        }

        return documentsList;
    }

    public void updateData(String collectionName, String documentId, Map<String, Object> updatedData)
        throws ExecutionException, InterruptedException {

        // Reference to the Firestore collection
        CollectionReference collection = db.collection(collectionName);

        // Reference to the document to update
        DocumentReference docRef = collection.document(documentId);

        // Update the document
        ApiFuture<WriteResult> future = docRef.set(updatedData, SetOptions.merge());

        // Wait for the update to complete
        future.get();
    }

    public void addSubDocument(String collection, String document, String subCollection, String subDocument, Map<String, Object> data)throws ExecutionException, InterruptedException {
            DocumentReference docRef = db.collection(collection).document(document).collection(subCollection).document(subDocument);
            ApiFuture<WriteResult> result = docRef.set(data);
             result.get(); 
    }
    public void addSubDocumentPatient(String collection0,String document0,String subcollection1,String subdocument1,String subcollection2,String subdocument2,String subcollection3,String subdocument3,Map<String, Object> data)throws ExecutionException, InterruptedException {
            DocumentReference docRef = db.collection(collection0).document(document0).collection(subcollection1).document(subdocument1).collection(subcollection2).document(subdocument2).collection(subcollection3).document(subdocument3);
            ApiFuture<WriteResult> result = docRef.set(data);
            result.get();
    }


    public List<String> getHospitalNames() {
        Firestore db = FirestoreClient.getFirestore();
        CollectionReference hospitals = db.collection("hospitals");

        ApiFuture<QuerySnapshot> query = hospitals.get();
        List<String> hospitalNames = new ArrayList<>();
        try {
            QuerySnapshot querySnapshot = query.get();
            querySnapshot.getDocuments().forEach(document -> {
                hospitalNames.add(document.getString("hospital name"));
            });
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return hospitalNames;
    }
    public DocumentSnapshot  getHospitalData(String hospitalName,LocalDate date ){
        Firestore db = FirestoreClient.getFirestore();
        DocumentReference docRef = db.collection("hospitals").document(hospitalName).collection("dates").document(date.toString());
        try{
           ApiFuture<DocumentSnapshot> future = docRef.get();
           return future.get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } 
    }


    public List<Map<String, Object>> getAvailableDoctors(String hospitalName, LocalDate date) throws ExecutionException, InterruptedException {
        List<Map<String, Object>> doctorsList = new ArrayList<>();
        DocumentReference dateDocRef = db.collection("hospitals").document(hospitalName).collection("dates").document(date.toString());
        ApiFuture<DocumentSnapshot> future = dateDocRef.get();
        DocumentSnapshot dateDoc = future.get();
    
        if (dateDoc.exists()) {
            List<Map<String, Object>> doctors = (List<Map<String, Object>>) dateDoc.get("Doctors");
            if (doctors != null) {
                doctorsList.addAll(doctors);
            }
        }
        return doctorsList; 
    }
    public boolean isCollectionExistsInDocument(String collectionName) throws ExecutionException, InterruptedException { 
             DocumentReference docRef = db.collection("patients").document(collectionName);
             System.out.println(collectionName);
                 DocumentSnapshot doc = docRef.get().get();
                 return doc.contains(collectionName);
      }

    // public long countDocuments(String collectionName) throws ExecutionException, InterruptedException {
    //     // Get a reference to the collection
    //     CollectionReference collection = db.collection("patient").document(collectionName).collection("hospitalLists");

    //     // Execute the query
    //     ApiFuture<QuerySnapshot> querySnapshot = collection.get();

    //     // Get query results (QuerySnapshot)
    //     QuerySnapshot snapshot = querySnapshot.get();

    //     // Get the count of documents
    //     long count = snapshot.size();
        
    //     return count;
    // }
       

    public List<DocumentSnapshot> getPatientAppointments(String userKey) throws Exception {
        List<DocumentSnapshot> appointments = new ArrayList<>();

        // Assuming the root collection is "patient"
        CollectionReference patientsRef = db.collection("patient");

        // Get the patient document
        DocumentReference patientDocRef = patientsRef.document(userKey);
        ApiFuture<DocumentSnapshot> patientDocFuture = patientDocRef.get();
        DocumentSnapshot patientDoc = patientDocFuture.get();

        if (patientDoc.exists()) {
            // Navigate to the nested collections
            CollectionReference hospitalsRef = patientDocRef.collection("hospitalList");
            ApiFuture<QuerySnapshot> querrSnap = hospitalsRef.get();
            QuerySnapshot snaps = querrSnap.get();
            List<QueryDocumentSnapshot> hospitalsDocs = snaps.getDocuments();
            System.out.println("Number of hospitals: " + hospitalsDocs.size());

            for (QueryDocumentSnapshot hospitalDoc : hospitalsDocs) {
                CollectionReference datesRef = hospitalDoc.getReference().collection("date");
                ApiFuture<QuerySnapshot> datesFuture = datesRef.get();
                List<QueryDocumentSnapshot> datesDocs = datesFuture.get().getDocuments();

                for (QueryDocumentSnapshot dateDoc : datesDocs) {
                    CollectionReference timeSlotsRef = dateDoc.getReference().collection("timeSlot");
                    ApiFuture<QuerySnapshot> timeSlotsFuture = timeSlotsRef.get();
                    List<QueryDocumentSnapshot> timeSlotsDocs = timeSlotsFuture.get().getDocuments();

                    for (QueryDocumentSnapshot timeSlotDoc : timeSlotsDocs) {
                        appointments.add(timeSlotDoc);
                    }
                }
            }
        }

        return appointments;
    }
    
}
