package com.c2w.patient_dashboard;



import com.c2w.Controller.AppNavigation;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class Previous_Appointments {
    private AppNavigation app;
    private Pane view;
    private Image bgImage;
    private BackgroundImage backgroundImage;
    Button book_new_appointment;
    Button upcoming_appointment;
    Button previous_appointment;

    public Previous_Appointments(AppNavigation app) {
        this.app = app;
        initialize();
    }

    public Pane getView() {
        return view;
    }

    public void initialize(){
        
       bgImage = new Image("images/doc4.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT,
        BackgroundRepeat.NO_REPEAT,
        BackgroundPosition.DEFAULT,
        new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
        
       Background background = new Background(backgroundImage);
       
       book_new_appointment = new Button("Book New Appointment");
       upcoming_appointment = new Button("Upcoming Appointments");
       previous_appointment = new Button("Previous Appointments");
       book_new_appointment.setPrefWidth(640);
       upcoming_appointment.setPrefWidth(640);
       previous_appointment.setPrefWidth(640);
       book_new_appointment.setPrefHeight(55);
       upcoming_appointment.setPrefHeight(55);
       previous_appointment.setPrefHeight(55);
       book_new_appointment.setStyle("-fx-font-weight:bold");
       upcoming_appointment.setStyle(" -fx-font-weight:bold");
       previous_appointment.setStyle("-fx-font-weight:bold");
       book_new_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToBook();
        }  
       });
       upcoming_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToUpcoming();
        }  
       });


       
       HBox hBox = new HBox(book_new_appointment,upcoming_appointment,previous_appointment);
       hBox.setLayoutY(993);
    
        
        
        view = new Pane(hBox);
        view.setBackground(background);

    }

    
}

