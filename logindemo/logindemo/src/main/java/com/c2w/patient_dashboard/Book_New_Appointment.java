package com.c2w.patient_dashboard;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.c2w.Controller.AppNavigation;
import com.c2w.firebaseConfig.DataService;
import com.c2w.signup.HospitalLogin;
import com.google.cloud.firestore.DocumentSnapshot;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


public class Book_New_Appointment {
    private AppNavigation app;
    private Pane view;
    Label hello ;
    Image bgImage;
    BackgroundImage backgroundImage ;
    Button book_new_appointment;
    Button upcoming_appointment;
    Button previous_appointment;
    Button availabel_doctor;
    Button availabel_appointment;
    Button ahead_appointment;
    Label fill_details;
    Label name;
    Label age;
    Label sex;
    Label issue;
    Label bloodgroup;
    TextField nameField;
    TextField ageField;
    TextField sexField;
    TextField patientNumberField;
    TextArea issueArea;
    TextField bloodField;
    Button confirm_appointment;
    Button logout;
    Button aboutus;
    TextField textField;
    Dialog<String> dialog;
    ToggleGroup sexToggleGroup;
    ComboBox<String> maritalStatusComboBox;
    private String key;

    ChoiceBox<String> select_hospital;
    ChoiceBox<String> select_time_slot;
    DatePicker datePicker;
    DataService dataService;
    Upcoming_Appointments upcoming_Appointments;

    public Book_New_Appointment(AppNavigation app) {
        this.app = app;
       // initialize();
    }
    public Pane getView() {
        return view;
    }

    public void initialize() {
    
      this.dataService = new DataService();
    
       bgImage = new Image("images/d3.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
       Background background = new Background(backgroundImage);
    
       book_new_appointment = new Button("Book New Appointment");
       upcoming_appointment = new Button("Upcoming Appointments");
       previous_appointment = new Button("Previous Appointments");
       book_new_appointment.setPrefWidth(640);
       upcoming_appointment.setPrefWidth(640);
       previous_appointment.setPrefWidth(640);
       book_new_appointment.setPrefHeight(55);
       upcoming_appointment.setPrefHeight(55);
       previous_appointment.setPrefHeight(55);
       book_new_appointment.setStyle("-fx-font-weight:bold");
       upcoming_appointment.setStyle(" -fx-font-weight:bold");
       previous_appointment.setStyle("-fx-font-weight:bold");
       
       HBox hBox = new HBox(book_new_appointment,upcoming_appointment,previous_appointment);
       hBox.setLayoutY(993);

       previous_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToPreviousAppoint();
        }  
       });
    upcoming_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToUpcoming();
        }  
       });
       
       select_hospital = new ChoiceBox<>();
       select_hospital.getItems().add("Select Hospital");
       select_hospital.setValue("Select Hospital");
       select_hospital.setPrefHeight(35);
       select_hospital.setPrefWidth(400);
       select_hospital.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");

       List<String> hospitalNames = dataService.getHospitalNames();
        select_hospital.getItems().addAll(hospitalNames);

        select_hospital.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            updateAvailableTimeSlots();
            setAvailableDoctors();
            
        });
       
       select_time_slot = new ChoiceBox<>();
       select_time_slot.getItems().addAll("Select Time Slot");
       select_time_slot.setValue("Select Time Slot");
       select_time_slot.setPrefHeight(35);
       select_time_slot.setPrefWidth(175);
       select_time_slot.setStyle("-fx-background-color:Pink; -fx-font-weight:bold");


       datePicker = new DatePicker();
       datePicker.setValue(LocalDate.now());
       datePicker.setPrefWidth(175);
       datePicker.setPrefHeight(35);
       datePicker.setStyle("-fx-font-weight:bold");

       datePicker.valueProperty().addListener((observable, oldValue, newValue) -> {
        updateAvailableTimeSlots();
        setAvailableDoctors();
        
    });


     textField = new TextField();
     dialog = new Dialog<String>();
     dialog.setTitle("Available Doctors");
     ButtonType type = new ButtonType("Ok", ButtonData.OK_DONE);
     dialog.setContentText("Please Select Hospital and Date!");
     dialog.getDialogPane().getButtonTypes().add(type);
     dialog.setWidth(500);
     dialog.setHeight(500);
       
       availabel_doctor = new Button("Check Availabel Doctors");
       availabel_appointment = new Button("Check Availabel appointments in slot");
       ahead_appointment = new Button(" Check Appointments ahead of you in slot");
       availabel_doctor.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");
       availabel_appointment.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");
       ahead_appointment.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");

       
       availabel_doctor.setOnAction(e -> extracted(textField, dialog));
       
       
       availabel_doctor.setPrefHeight(35);
       availabel_appointment.setPrefHeight(35);
       ahead_appointment.setPrefHeight(35);
       
       fill_details = new Label("Fill Details : ");
       fill_details.setFont(new Font("Cooper Black", 35));
       name = new Label("Name");
       age = new Label("Age");
       sex = new Label("Sex");
       issue = new Label("Issue");
       bloodgroup = new Label("Blood Group");

       nameField = new TextField();
       nameField.setPrefWidth(345);
       nameField.setPrefHeight(35);
       ageField = new TextField();
       ageField.setPrefWidth(102);
       ageField.setPrefHeight(35);
       bloodField = new TextField();
       bloodField.setPrefWidth(102);
       bloodField.setPrefHeight(35);
       issueArea = new TextArea();
       HBox areaHBox = new HBox(issueArea);
       areaHBox.setPrefHeight(100);
        
        Label patientNumberLabel = new Label("Mobile Number");
        patientNumberField = new TextField();
        patientNumberField.setPrefHeight(35);

        Label maritalStatusLabel = new Label("Marital Status");
        maritalStatusComboBox = new ComboBox<>();
        maritalStatusComboBox.getItems().addAll("Select","Single", "Married", "Divorced", "Widowed", "Other");
        maritalStatusComboBox.setValue("Select");
        maritalStatusComboBox.setPrefHeight(35);
        
        HBox labelBox = new HBox(10,patientNumberLabel,patientNumberField);
        HBox labelBox2 = new HBox(24,maritalStatusLabel,maritalStatusComboBox);

        sexToggleGroup = new ToggleGroup();
        RadioButton maleRadioButton = new RadioButton("Male");
        maleRadioButton.setToggleGroup(sexToggleGroup);
        RadioButton femaleRadioButton = new RadioButton("Female");
        femaleRadioButton.setToggleGroup(sexToggleGroup);
        RadioButton otherRadioButton = new RadioButton("Other");
        otherRadioButton.setToggleGroup(sexToggleGroup);

       HBox radioHBox = new HBox(5,maleRadioButton,femaleRadioButton,otherRadioButton);
       HBox sexHBox = new HBox(31,sex,radioHBox);
       
       confirm_appointment = new Button("Confirm Appointment");
       confirm_appointment.setPrefHeight(35);
       
       Button clearButton = new Button("Clear");
       clearButton.setPrefHeight(35);

       clearButton.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            nameField.clear();
            ageField.clear();
            patientNumberField.clear();
            issueArea.clear();
            bloodField.clear();  
            sexToggleGroup.selectToggle(null);   
            maritalStatusComboBox.setValue(null);
        }
        
       });
       confirm_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
             handleConfirmAppointment();
             upcoming_Appointments = new Upcoming_Appointments();
        }
        
       });

    



       HBox clear_confirm = new HBox(10,confirm_appointment,clearButton);
       clear_confirm.setPrefWidth(345);
       clear_confirm.setAlignment(Pos.CENTER);

       HBox nameHBox = new HBox(15,name,nameField);
       HBox ageHBox = new HBox(28,age,ageField,bloodgroup,bloodField);
       VBox issueVBox = new VBox(issue);
       areaHBox.setPrefWidth(345);
       HBox issueHBox = new HBox(20,issueVBox,areaHBox);


       HBox date_time_hBox = new HBox(50,datePicker,select_time_slot);
       VBox drop_list_vBox = new VBox(30,select_hospital,date_time_hBox,availabel_doctor,availabel_appointment,ahead_appointment,fill_details,nameHBox,ageHBox,sexHBox,labelBox,labelBox2,issueHBox,clear_confirm);
       drop_list_vBox.setPrefWidth(400);
       drop_list_vBox.setAlignment(Pos.CENTER);
       drop_list_vBox.setLayoutY(105);
       drop_list_vBox.setLayoutX(1000);

       logout = new Button("Logout");
       logout.setPrefHeight(35);
       logout.setLayoutX(1845);
       logout.setLayoutY(5);
       aboutus = new Button("About Us");
       aboutus.setPrefHeight(35);
       aboutus.setLayoutX(1750);
       aboutus.setLayoutY(5);

       logout.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            app.NavigateToLogin();
        }
        
       });

       Text hello = getLoginName();
       hello.setLayoutY(15);
       hello.setLayoutX(5);
       hello.setFont(new Font("Cooper Black", 55));
       hello.setFill(Color.BROWN);

       HBox welcomeHBox = new HBox(hello);
       welcomeHBox.setPrefWidth(1920);
       welcomeHBox.setAlignment(Pos.CENTER);

       
    
       view = new Pane(welcomeHBox,hBox,drop_list_vBox,logout,aboutus);
       view.setBackground(background);
       
    }  

    public Text getLoginName(){
        Label dataLabel = new Label();
        String patientName = "default name";
        String yashkey = null;
        key = HospitalLogin.keygiver(yashkey);
        System.out.println(key);
        try{
            if (key != null) {
                DocumentSnapshot dataObject = dataService.getData("patient", key);
                patientName = dataObject.getString("username");
            } else {
                System.out.println("Key is null.");
            }
            dataLabel.setText(patientName);
        }catch(Exception e){
             e.printStackTrace();
        }
        return new Text("Hello "+dataLabel.getText());
    }

     private void updateAvailableTimeSlots() {
        String selectedHospital = select_hospital.getValue();
        LocalDate selectedDate = datePicker.getValue();
        System.out.println(selectedHospital);
        System.out.println(selectedDate);
        

        if (selectedHospital != null && !selectedHospital.equals("Select Hospital") && selectedDate != null) {
            DocumentSnapshot hospitalData = dataService.getHospitalData(selectedHospital, selectedDate);

            if (hospitalData != null) {
                String openingTime = hospitalData.getString("openingTime");
                String closingTime = hospitalData.getString("closingTime");
                System.out.println(openingTime);
                System.out.println(closingTime);

                if (openingTime != null && closingTime != null) {
                    List<String> timeSlots = calculateTimeSlots(openingTime, closingTime);
                    System.out.println(timeSlots.toString());
                    select_time_slot.getItems().clear();
                    select_time_slot.getItems().add("Select Time Slot");
                    select_time_slot.setValue("Select Time Slot");
                    select_time_slot.getItems().addAll(timeSlots);
                    select_time_slot.setValue("Select Time Slot");
                }
            }
        }
    }

    public void setAvailableDoctors() {
        String selectedHospital = select_hospital.getValue();
        LocalDate selectedDate = datePicker.getValue();
        System.out.println(selectedHospital);
        System.out.println(selectedDate);
        if (selectedHospital != null && !selectedHospital.equals("Select Hospital") && selectedDate != null) {
            showAvailableDoctors(selectedHospital, selectedDate);
        }   
    }

    private List<String> calculateTimeSlots(String openingTime, String closingTime) {
        List<String> timeSlots = new ArrayList<>();

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime startTime = LocalTime.parse(openingTime.toUpperCase(), timeFormatter);
        LocalTime endTime = LocalTime.parse(closingTime.toUpperCase(), timeFormatter);

        while (startTime.isBefore(endTime)) {
            LocalTime slotEndTime = startTime.plusHours(2);
            if (slotEndTime.isAfter(endTime)) {
                slotEndTime = endTime;
            }

            timeSlots.add(startTime.format(timeFormatter) + "-" + slotEndTime.format(timeFormatter));
            startTime = slotEndTime;
        }

        return timeSlots;
    }
    private void extracted(TextField textField, Dialog<String> dialog) {
        dialog.showAndWait();
    }

     public void showAvailableDoctors(String hospitalName, LocalDate date) {
        try {
            List<Map<String, Object>> doctors = dataService.getAvailableDoctors(hospitalName, date);
            if (doctors.isEmpty()) {
               dialog.setContentText("There are no available doctors for the selected date.");
            
            } else {
                ListView<String> doctorsListView = new ListView<>();
                for (Map<String, Object> doctor : doctors) {
                    String doctorInfo = "ID: " + doctor.get("ID") + ", Name: " + doctor.get("Name") + ", Specialization: " + doctor.get("Specialization") + ", Degree: " + doctor.get("Degree");
                    doctorsListView.getItems().add(doctorInfo);
                }
                
               dialog.getDialogPane().setContent(doctorsListView); 
               dialog.getDialogPane().setPrefSize(500, 500);                             
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void handleConfirmAppointment() {
        LocalDate date = datePicker.getValue();
        String selectedDate = date.toString();
        String selectedHospital = select_hospital.getValue();
        String selectedTimeSlot = select_time_slot.getValue();
        String patientName =  nameField.getText();
        String age = ageField.getText();
        String bloodGroup = bloodField.getText();
        String phone = patientNumberField.getText();
        String status = maritalStatusComboBox.getValue();
        String issues = issueArea.getText();
        String userName = key;
        String collecName = date + selectedTimeSlot; 
        RadioButton selectedSexRadioButton = (RadioButton) sexToggleGroup.getSelectedToggle();
        if (selectedHospital == null || selectedHospital.equals("Select Hospital") ||selectedDate == null || selectedTimeSlot == null || selectedTimeSlot.equals("Select Time Slot") ||patientName.isEmpty() || selectedSexRadioButton == null) {
        Alert alert = new Alert(AlertType.ERROR, "Please fill all the required fields.");
        alert.show();
        return;
       }
        String sexOfPatient = selectedSexRadioButton.getText();
        if(key!=null) {
            try {
                Map<String, Object> appointmentDetails = new HashMap<>();
                appointmentDetails.put("date",selectedDate);
                appointmentDetails.put("name",patientName);
                appointmentDetails.put("hospitalName",selectedHospital);
                appointmentDetails.put("time slot",selectedTimeSlot);
                appointmentDetails.put("age",age);
                appointmentDetails.put("bloodGroup",bloodGroup);
                appointmentDetails.put("sex", sexOfPatient);
                appointmentDetails.put("phoneNo", phone);
                appointmentDetails.put("maritalStatus",status);
                appointmentDetails.put("issue",issues);
                dataService.addSubDocumentPatient("patients",userName,"hospitalLists",selectedHospital,"date",selectedDate,"timeslot",selectedTimeSlot,appointmentDetails); 
               System.out.println("appointment details are succesfully added for date"+selectedDate);
                clearInputFields();
            }catch(Exception ex) {
                ex.printStackTrace();
            }
        }else {
            System.out.println("User Not Found");
        } 
    }


  private void clearInputFields() {
      nameField.clear();
      ageField.clear();
      bloodField.clear();
      patientNumberField.clear();
      issueArea.clear();
      sexToggleGroup.selectToggle(null);
      maritalStatusComboBox.setValue("Select");
      select_hospital.setValue("Select Hospital");
      select_time_slot.setValue("Select Time Slot");
     datePicker.setValue(LocalDate.now());
   }
}
