package com.c2w.patient_dashboard;


import java.util.List;
import java.util.Map;

import com.c2w.Controller.AppNavigation;
import com.c2w.firebaseConfig.DataService;
import com.c2w.signup.HospitalLogin;
import com.google.cloud.firestore.DocumentSnapshot;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;


public class Upcoming_Appointments {
    private AppNavigation app;
    private Pane view;
    private Image bgImage;
    private BackgroundImage backgroundImage;
    Button book_new_appointment;
    Button upcoming_appointment;
    Button previous_appointment;
    ScrollPane scrollPane;
    DataService dataService;

    public Upcoming_Appointments(AppNavigation app) {
        this.app = app;
        //initialize();
    }
    public Upcoming_Appointments() {
        initialize();
    }

    public Pane getView() {
        return view;
    }

    public void initialize(){
        this.dataService = new DataService();
       bgImage = new Image("images/d3.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
       Background background = new Background(backgroundImage);

       


       book_new_appointment = new Button("Book New Appointment");
       upcoming_appointment = new Button("Upcoming Appointments");
       previous_appointment = new Button("Previous Appointments");
       book_new_appointment.setPrefWidth(640);
       upcoming_appointment.setPrefWidth(640);
       previous_appointment.setPrefWidth(640);
       book_new_appointment.setPrefHeight(55);
       upcoming_appointment.setPrefHeight(55);
       previous_appointment.setPrefHeight(55);
       book_new_appointment.setStyle("-fx-font-weight:bold");
       upcoming_appointment.setStyle(" -fx-font-weight:bold");
       previous_appointment.setStyle("-fx-font-weight:bold");

       previous_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToPreviousAppoint();
        }
       });
       book_new_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToBook();
        }
       });
       
       HBox hBox = new HBox(book_new_appointment,upcoming_appointment,previous_appointment);
       hBox.setLayoutY(993);


        scrollPane = new ScrollPane();
        scrollPane.setPrefSize(1100,800);
        scrollPane.setLayoutY(100);
        scrollPane.setLayoutX(600);
    
        VBox appointmentsContainer = new VBox(10);
        appointmentsContainer.setPadding(new Insets(10));
        appointmentsContainer.setAlignment(Pos.CENTER);
        //loadAppointments(appointmentsContainer);
        scrollPane.setContent(appointmentsContainer);
        
        // Load appointments and add them to the appointments container
        loadAppointments(appointmentsContainer);
        
        view = new Pane(hBox,scrollPane);
        view.setBackground(background);

    }

    private void loadAppointments(VBox container) {
        String setkey = null;
        String userKey = HospitalLogin.keygiver(setkey);
        System.out.println(userKey);
        if (userKey != null) {
            System.out.println(userKey);
            try {
                System.out.println("Is collection present? : "+dataService.isCollectionExistsInDocument(userKey));
                List<DocumentSnapshot> appointmentDocuments = dataService.getPatientAppointments(userKey);
                for (DocumentSnapshot doc : appointmentDocuments) {
                    Map<String, Object> appointment = doc.getData();
                    if (appointment != null) {
                        VBox appointmentCard = createAppointmentCard(appointment);
                        container.getChildren().add(appointmentCard);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private VBox createAppointmentCard(Map<String, Object> appointment) {
        VBox card = new VBox();
        card.setStyle("-fx-background-color: #ffffff; -fx-border-color: #000000; -fx-padding: 10px;");

        Label dateLabel = new Label("Date: " + appointment.get("date"));
        Label hospitalLabel = new Label("Hospital: " + appointment.get("hospitalName"));
        Label timeSlotLabel = new Label("Time Slot: " + appointment.get("timeSlot"));
        Label patientNameLabel = new Label("Name: " + appointment.get("name"));
        Label ageLabel = new Label("Age: " + appointment.get("age"));
        Label bloodGroupLabel = new Label("Blood Group: " + appointment.get("bloodGroup"));
        Label sexLabel = new Label("Sex: " + appointment.get("sex"));
        Label phoneLabel = new Label("Phone No: " + appointment.get("phoneNo"));
        Label maritalStatusLabel = new Label("Marital Status: " + appointment.get("maritalStatus"));
        Label issueLabel = new Label("Issue: " + appointment.get("issue"));

        card.getChildren().addAll(dateLabel, timeSlotLabel, hospitalLabel, patientNameLabel, ageLabel, bloodGroupLabel, sexLabel, phoneLabel, maritalStatusLabel, issueLabel);
        return card;
    }
}

    
    

