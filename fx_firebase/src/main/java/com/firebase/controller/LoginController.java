package com.firebase.controller;

import java.io.FileInputStream;
import java.io.IOException;

import com.firebase.firebase_connection.FirebaseService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LoginController extends Application {

    private Stage primarStage;
    private FirebaseService firebaseService;
    private Scene scene;

    public void setPrimaryStageScene(Scene scene) {
        primarStage.setScene(scene);
    }
    public void initializeLoginScene() {
        Scene logiScene = createLoginScene();
        this.scene = logiScene;
        primarStage.setScene(logiScene);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primarStage = primaryStage;
        try{
            FileInputStream serviceAccount = new FileInputStream("fx_firebase/src/main/resources/firebase.json");

            @SuppressWarnings("deprecation")
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://javafx-883bb-default-rtdb.asia-southeast1.firebasedatabase.app")
                .build();
                FirebaseApp.initializeApp(options);
        }catch (IOException e) {
            e.printStackTrace();
        }

        Scene scene = createLoginScene();
        this.scene = scene;
        primaryStage.setTitle("Firebase Auth Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private Scene createLoginScene() {
         TextField emailField = new TextField();
         emailField.setPromptText("Email");

         PasswordField passwordField = new PasswordField();
         passwordField.setPromptText("Password");

         Button signUpButton = new Button("Sign Up");
         Button logiButton = new Button("Log In");

         firebaseService = new FirebaseService(this,emailField,passwordField);
         signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.signUp();
            }
            
         });
         logiButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.login();
            }
            
         });
         VBox fieldBox = new VBox(20,emailField,passwordField);
         HBox buttBox = new HBox(20,logiButton,signUpButton);
         VBox comBox = new VBox(10,fieldBox,buttBox);
         Pane viewPane = new Pane(comBox);

         return new Scene(viewPane,400,200);
        }
    
}
