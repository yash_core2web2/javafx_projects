package com.firestore.controller;

import com.firestore.dashboards.UserPage; import com.firestore.firebaseConfig.DataService; import javafx.event.ActionEvent; import javafx.event.EventHandler; import javafx.geometry.Pos; import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.concurrent.ExecutionException;public class LoginController {

 private Stage primaryStage; // The primary stage for displaying scenes
 private Scene loginScene; // Scene for the login page
 private Scene userScene; // Scene for the user dashboard
 private DataService dataService; // Service to interact with Firestore
 public static String key; // Static key to store the logged-in username

 // Constructor to initialize the LoginController with the primary stage
 public LoginController(Stage primaryStage) {
 this.primaryStage = primaryStage;
 dataService = new DataService(); //Initialize DataService instance
 initScenes(); // Initialize scenes
 }

 // Method to initialize all scenes
 private void initScenes() {
 initLoginScene(); // Initialize the login scene
 }

 // Method to initialize the login scene
 private void initLoginScene() {
 Label userLabel = new Label("Username");
    
    // Label for username
 TextField userTextField = new TextField();
    // TextField for username input
 Label passLabel = new Label("Password");
    // Label for password
 PasswordField passField = new
    PasswordField(); // PasswordField for password input

 Button loginButton = new Button("Login");
    // Button to trigger login action
 Button signupButton = new Button("Signup");
    // Button to navigate to signup scene

 // Set action for the login button
 loginButton.setOnAction(new
    EventHandler<ActionEvent>() {
 @Override
 public void handle(ActionEvent event) {

    handleLogin(userTextField.getText(),
    passField.getText()); // Handle login
    userTextField.clear(); // Clearusername field
    passField.clear(); // Clear password field
 }
 });

 // Set action for the signup button
 signupButton.setOnAction(new
    EventHandler<ActionEvent>() {
    
 @Override
 public void handle(ActionEvent event) {
 showSignupScene(); // Show signup scene
 userTextField.clear(); // Clear username field
 passField.clear(); // Clear password field
 }
 });

 // Style the labels
 userLabel.setStyle("-fx-text-fill:white;");
 passLabel.setStyle("-fx-text-fill:white;");

 // Create VBox layouts for the fields and buttons
 VBox fieldBox1 = new VBox(10, userLabel,
    userTextField);
 fieldBox1.setMaxSize(300, 30);
 VBox fieldBox2 = new VBox(10, passLabel,
    passField);
 fieldBox2.setMaxSize(300, 30);
 HBox buttonBox = new HBox(50, loginButton,signupButton);
 buttonBox.setMaxSize(350, 30);
 buttonBox.setAlignment(Pos.CENTER);

    
 userTextField.setStyle("-fx-set-pref-width:350");
    
 passField.setStyle("-fx-set-pref-width:350");
    

 VBox vbox = new VBox(20, fieldBox1,
    fieldBox2, buttonBox);
    
 vbox.setStyle("-fx-background-image:url('https://img.freepik.com/free-photo/digital-world-banner-background-remixed-from-public-domain-by-nasa_53876-124622.jpg?semt=sph')");
 vbox.setAlignment(Pos.CENTER);
 loginScene = new Scene(vbox, 600, 600);
 }

 // Method to initialize the user scene
 private void initUserScene() {
 UserPage userPage = new UserPage(dataService); // Create UserPage instance
 userScene = new Scene(userPage.createUserScene(this::handleLogout),600, 600); // Create user scene
 }

 // Method to get the login scene
 public Scene getLoginScene() {
 return loginScene;
     }
    
     // Method to show the login scene
     public void showLoginScene() {
     primaryStage.setScene(loginScene);
     primaryStage.setTitle("Login");
    
     primaryStage.show();
     }
    
     // Method to handle login action
     private void handleLogin(String username,
    String password) {
     try {
     // Authenticate user
     if
    (dataService.authenticateUser(username, password))
    {
     key = username; // Store the username in the static key
     initUserScene(); // Initialize user scene
     primaryStage.setScene(userScene);
    // Show user scene
     primaryStage.setTitle("User Dashboard");
     } else {
     System.out.println("Invalid credentials");
     key = null;
     }
     } catch (ExecutionException |
    InterruptedException ex) {
     ex.printStackTrace();
     }
     }
    
     // Method to show the signup scene
     private void showSignupScene() {
    
     SignupController signupController = new
    SignupController(this); // Create SignupControllerinstance 
    Scene signupScene = signupController.createSignupScene(primaryStage);
    // Create signup scene
     primaryStage.setScene(signupScene);
     primaryStage.setTitle("Signup");
     primaryStage.show();
     }
    
     // Method to handle logout action
     private void handleLogout() {
     primaryStage.setScene(loginScene); //Show login scene
     primaryStage.setTitle("Login");
     }
    }