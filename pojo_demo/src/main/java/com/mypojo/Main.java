package com.mypojo;

import java.util.Scanner;

import com.mypojo.simplePojo.PlayersData;

public class Main {
    public static void main(String[] args) {
         System.out.println("Player Info : ");
        PlayersData pd = new PlayersData();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Player Name : ");
        String playerName = sc.nextLine();
        System.out.println("Enter Team Name : ");
        String teamName = sc.nextLine();
        System.out.println("Enter Jersey Number : ");
        int jerseyNo = sc.nextInt();

        pd.setPlayerName(playerName);
        pd.setJerseyNo(jerseyNo);
        pd.setTeamName(teamName);

        System.out.println("Player Name : "+pd.getPlayerName());
        System.out.println("Team Name : "+pd.getTeamName());
        System.out.println("Jersey Number : "+pd.getJerseyNo());
        sc.close();
    }
}