package com.mypojo.simplePojo;

public class PlayersData {
    private String playerName;
    private String teamName;
    private int jerseyNo;
    public String getPlayerName() {
        return playerName;
    }
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
    public String getTeamName() {
        return teamName;
    }
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
    public int getJerseyNo() {
        return jerseyNo;
    }
    public void setJerseyNo(int jerseyNo) {
        this.jerseyNo = jerseyNo;
    }

    

}
