package com.my_form_app.controller;

import com.my_form_app.dataAccess.FormPage1;
import com.my_form_app.dataAccess.FormPage2;
import com.my_form_app.dataAccess.FormPage3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FormNavigationApp extends Application {
    private Stage primaryStage;
    private Scene page1Scene,page2Scene,page3Scene;

    private FormPage1 page1;
    private FormPage2 page2;
    private FormPage3 page3;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        page1 = new FormPage1(this);
        page2 = new FormPage2(this);
        page3 = new FormPage3(this);

       page1Scene = new Scene(page1.getView(),500,500);
       page2Scene = new Scene(page2.getView(),500,500);
       page3Scene = new Scene(page3.getView(),500,500);
        // primaryStage.setResizable(false);
        primaryStage.setScene(page1Scene);
        primaryStage.setTitle("Form Navigation");
        primaryStage.show();
    }

    public void NavigatetoPage1() {
        page2.setField2value(page2.getField2Value());
       // page1.setField1value(page1.getField1Value());
        primaryStage.setScene(page1Scene);
    }

    public void NavigatetoPage2() {
        page1.setField1value(page1.getField1Value());
        page3.setField3value(page3.getField3Value());
        primaryStage.setScene(page2Scene);
    }

    public void NavigatetoPage3() {
        page2.setField2value(page2.getField2Value());
        primaryStage.setScene(page3Scene);
    }




    
    
}
