package com.my_form_app;

import com.my_form_app.controller.FormNavigationApp;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Application.launch(FormNavigationApp.class,args);
    }
}