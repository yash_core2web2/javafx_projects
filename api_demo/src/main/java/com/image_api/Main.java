package com.image_api;

import com.image_api.service.APIService;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
      Application.launch(APIService.class,args);
    }
}